package io.jichay.sdvshadow.service;

import com.azure.core.http.policy.HttpLogDetailLevel;
import com.azure.core.management.AzureEnvironment;
import com.azure.core.management.profile.AzureProfile;
import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
import com.azure.resourcemanager.AzureResourceManager;
import com.azure.resourcemanager.compute.models.VirtualMachine;
import org.springframework.stereotype.Service;

@Service
public class AzureVMService {

    private final AzureProfile profile = new AzureProfile(AzureEnvironment.AZURE);

    private final ClientSecretCredential clientSecretCredential = new ClientSecretCredentialBuilder()
            .clientId("")
            .clientSecret("")
            .tenantId("")
            .build();

    private final AzureResourceManager azureResourceManager = AzureResourceManager
            .configure()
            .withLogLevel(HttpLogDetailLevel.BASIC)
            .authenticate(clientSecretCredential, profile)
            .withSubscription("");

    private final VirtualMachine vm = azureResourceManager.virtualMachines().getByResourceGroup("", "");


    public VirtualMachine startVM() {
        try {
            vm.start();
            return vm;
        }catch (Exception e) {
            return null;
        }
    }

}
