package io.jichay.sdvshadow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdvShadowApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdvShadowApplication.class, args);
	}

}
