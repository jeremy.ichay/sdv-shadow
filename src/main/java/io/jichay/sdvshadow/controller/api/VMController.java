package io.jichay.sdvshadow.controller.api;

import com.azure.resourcemanager.compute.models.VirtualMachine;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.jichay.sdvshadow.service.AzureVMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vm")
public class VMController {

    @Autowired
    AzureVMService azureVMService;

    @GetMapping("/start")
    public ObjectNode startVM() {
        ObjectMapper mapper = new ObjectMapper();
        VirtualMachine vm = azureVMService.startVM();
        ObjectNode node = mapper.createObjectNode();
        node.put("ip", vm.getPrimaryPublicIPAddress().ipAddress());
        node.put("user", "");
        node.put("pass", "");
        return node;
    }

}
