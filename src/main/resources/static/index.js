$(() => {

    var successGettingInfo = false;

    $("#game-start").on("click", () => {
        $("#errorInfo").addClass("d-none");
        $('#infoModal').modal("show");
        if(!successGettingInfo) {
            $.get( "/api/vm/start", (data) => {
                $("#spinner").hide();
                $("#vm-info-user").append(data.user);
                $("#vm-info-pass").append(data.pass);
                $("#vm-info-ip").append(data.ip);
                successGettingInfo = true;
            }).fail(function() {
                $("#spinner").hide();
                $("#errorInfo").removeClass("d-none");
            });
        }
    })

});